provider "aws" {
  region = "${var.AWS_REGION}"
  access_key = "${var.AWS_ACCESSKEY}"
  secret_key = "${var.AWS_SECRETKEY}"
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "${var.AWS_BUCKET_NAME}"

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }
}
