provider "digitalocean" {
  token = "${var.DO_TOKEN}"
}

resource "digitalocean_droplet" "worker" {
  image = "ubuntu-18-04-x64"
  name = "${format("worker%02d", count.index + 1)}"
  count = "${var.DO_WORKERCOUNT}"
  region = "${var.DO_REGION}"
  size = "${var.DO_SIZE}"
  private_networking = true
  ssh_keys = [
    "${var.DO_KEYFINGERPRINT}"
  ]
  connection {
    user = "root"
    type = "ssh"
    private_key = "${file("~/.ssh/id_rsa")}"
    timeout = "2m"
  }

  provisioner "file" {
    source = "../../scripts/workerscript.sh"
    destination = "/workerscript.sh"
  }

  provisioner "file" {
    source = "../../scripts/backup_to_volume.sh"
    destination = "/etc/backup_to_volume.sh"
  }

  provisioner "file" {
    source = "../../scripts/backup_crontab"
    destination = "/etc/cron.d/backup_crontab"
  }

  provisioner "remote-exec" {
    inline = [
      "sleep 10",

      "chmod +x /workerscript.sh",
      "chmod +x /etc/backup_to_volume.sh",
      "chmod +x /etc/cron.d/backup_crontab",

      "apt-get install zip -y",

      "nohup bash /workerscript.sh &",
      "sleep 2"
    ]
  }

}