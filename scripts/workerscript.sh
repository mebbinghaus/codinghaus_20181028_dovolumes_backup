#!/bin/bash
mkdir /workdir
touch workerscript.log
# wait until backup volume is mounted
loopCount=0
while [ ! -d /mnt/$HOSTNAME\_backup ]
do
    echo "waiting for DO-Volume to be mounted...." >> workerscript.log
    sleep 10
    ((loopCount+=10))
    if (( loopCount > 120 )); then
        echo "Volume not mounted after two minutes, trying manual mount..."  >> workerscript.log
        mkdir -p /mnt/$HOSTNAME\_backup; mount -o discard,defaults /dev/disk/by-id/scsi-0DO_Volume_$HOSTNAME-backup /mnt/$HOSTNAME\_backup; echo /dev/disk/by-id/scsi-0DO_Volume_$HOSTNAME-backup /mnt/$HOSTNAME\_backup ext4 defaults,nofail,discard 0 0 | sudo tee -a /etc/fstab
    fi
done
echo "DO-Volume is now mounted!" >> workerscript.log
# restore backup from volume to droplet if existing
newestBackup=$(ls -Frt /mnt/$HOSTNAME\_backup | grep "[^/]$" | tail -n 1)
if [ -z "$newestBackup" ]; then
    echo "No backup found on DO-Volume!" >> workerscript.log
else
    cp /mnt/$HOSTNAME\_backup/$newestBackup /workdir
    unzip /workdir/$newestBackup -d /workdir
    rm -rf /workdir/$newestBackup
    echo "Found backup ($newestBackup) on DO-Volume! Copied and unzipped it into working directory!" >> workerscript.log
fi
newestFile=$(ls -Frt /workdir | grep "[^/]$" | tail -n 1)
counter=0
if [ -z "$newestFile" ]; then
    echo "No previous file found. Starting with 1!" >> workerscript.log
    counter=1
else
    echo "Found file to start with! ($newestFile)" >> workerscript.log
    ((counter+=$newestFile))
    ((counter+=1))
fi
while [ 1 ]; do
    sleep 5
    fallocate -l 1M /workdir/$counter
    echo "Created file: $counter" >> workerscript.log
    ((counter+=1))
done