provider "aws" {
  region = "${var.AWS_REGION}"
  access_key = "${var.AWS_ACCESSKEY}"
  secret_key = "${var.AWS_SECRETKEY}"
}

terraform {
  backend "s3" {
    bucket = "de.codinghaus.s3"
    key = "dovolumetutorial_terraform.tfstate"
    region = "eu-central-1"
    access_key = ""
    secret_key = ""
  }
}
