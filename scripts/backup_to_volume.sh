#!/bin/bash
date=$(date +%s)
timestamp=$(date +"%Y_%m_%d_%H_%M")
if [ ! -d /mnt/$HOSTNAME\_backup ]; then
    echo "----- ZIPPING FILES AND COPYING ZIP TO DO-VOLUME (1/2) -----"
    cd /workdir
    zip -r $timestamp-backup.zip ./*
    mv $timestamp-backup.zip /mnt/$HOSTNAME\_backup

    echo "----- DELETING OUTDATED BACKUPS (2/2) -----"
    cd /mnt/$HOSTNAME\_backup
    ls -tp | grep -v '/$' | tail -n +6 | xargs -I {} rm -- {} #https://stackoverflow.com/questions/25785/delete-all-but-the-most-recent-x-files-in-bash
fi
