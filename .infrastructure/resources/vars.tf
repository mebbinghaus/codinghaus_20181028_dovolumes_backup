variable "DO_TOKEN" {}
variable "DO_KEYFINGERPRINT" {}
variable "DO_REGION" {}
variable "DO_SIZE" {}
variable "DO_VOLUME_SIZE" {}
variable "DO_VOLUME_FS_TYPE" {}
variable "DO_WORKERCOUNT" {}

variable "AWS_REGION" {}
variable "AWS_TF_STATEFILE" {}
variable "AWS_BUCKET_NAME" {}
variable "AWS_ACCESSKEY" {}
variable "AWS_SECRETKEY" {}