resource "digitalocean_volume" "worker-backup-volume" {
  count = "${var.DO_WORKERCOUNT}"
  region = "${var.DO_REGION}"
  name = "${format("worker%02d", count.index + 1)}-backup"
  size = "${var.DO_VOLUME_SIZE}"
  initial_filesystem_type = "${var.DO_VOLUME_FS_TYPE}"
  depends_on = ["digitalocean_droplet.worker"]

  lifecycle {
    prevent_destroy = true
  }
}

resource "digitalocean_volume_attachment" "worker-backup-volume-attachments" {
  count = "${var.DO_WORKERCOUNT}"
  droplet_id = "${element(digitalocean_droplet.worker.*.id, count.index)}"
  volume_id  = "${element(digitalocean_volume.worker-backup-volume.*.id, count.index)}"
  depends_on = ["digitalocean_volume.worker-backup-volume"]
}

